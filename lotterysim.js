if (Meteor.isClient) {
  Session.setDefault("plays", 0);
  Session.setDefault("wins", []);
  Session.setDefault("lastDrawn", []);

  //https://uk.finance.yahoo.com/news/is-the-new-2-pound-national-lottery-ticket-a-worse-bet-171015787.html
  Session.setDefault("winAmounts", [0, 0, 0, 25, 100, 1000, 3750000]);//50k for 5+bonus (not calcing bonus yet)
  Session.setDefault("playCost", 2);
  Session.setDefault("playingNow", false);

  Session.setDefault('speed', 0.05);

  UI.registerHelper('valAtPosOf', function (pos, arr) {
      return arr && arr[pos] || false;
  });

  Template.stats.lastDrawn = function(e, t) {
    var lastDrawn = Session.get('lastDrawn');
    if(!lastDrawn || !lastDrawn.length) {
      lastDrawn = false;
    }
    return lastDrawn;
  }
  Template.stats.wins = function(e, t) {
    return Session.get('wins');
  }
  Template.stats.costPerPlay = function(e, t) {
    return Session.get('playCost');
  }
  Template.stats.numberOfPlays = function(e, t) {
    return Session.get('plays');
  }
  Template.stats.picks = function(e, t) {
    return Session.get('picks');
  }
  Template.stats.helpers({
    sumByKey: function (arr, key) {
      var sum = 0;
      for(var pos = 0; pos < arr.length; pos++) {
        if(arr[pos] && arr[pos][key]) {
          sum += arr[pos][key];
        }
      }
      return sum || 0;
    },
    divide: function (a, b, r) {
      return ((a / b) || 0).toFixed(r||0);
    },
    multiply: function (a, b) {
      return (a * b) || 0;
    },
    isInArray: function(val, arr) {
      if(arr && arr.length > 0) {
        return (arr.indexOf(val) > -1)
      }
    }
  });

  function evalDraw(drawn) {
    var picks = Session.get('picks') || [];
    var match = drawn.filter(function(el, pos) {
      return picks.indexOf(el) != -1;
    })
    Session.set('lastDrawn', drawn);


    var wins = Session.get('wins');
    var winAmounts = Session.get('winAmounts');

    wins[match.length] = wins[match.length] || {
      name: 'match ' + (match.length || 'no') + ' balls',
      count: 0,
      amount: 0,
    };

    wins[match.length].count++;
    wins[match.length].amount += winAmounts[match.length];

    Session.set('wins', wins);

    if(winAmounts[match.length] > 1000000) {
      alert('YOU WON THE LOTTO AFTER ' + Session.get('plays') + ' DRAWS');
    }
  }

  function drawBalls() {
    var drawn = [];

    while(drawn.length < 6) {
      var rand = Math.floor((Math.random() * 49) + 1);
      if(drawn.indexOf(rand) == -1) {
        drawn.push(rand);
      }
    }

    drawn.sort(function(a, b) {
      return a > b ? 1 : -1;
    })

    return drawn;
  };
  Session.setDefault("picks", drawBalls());

  function playLottery(cb) {
    var plays = Session.get('plays');
    var drawn = drawBalls();

    evalDraw(drawn);

    plays++;
    Session.set('plays', plays);

    if(cb && typeof cb == 'function') {
      cb()
    }
  }

  Template.options.rendered = function(e, t) {
    if(Session.get('playingNow')) {
      Session.set('playingNow', false);
      $('a.startRun').trigger('click');
    }
  }
  Template.options.runText = function(e, t) {
    return Session.get('playingNow') ? 'Pause Simulation' : 'Run Simulation';
  }
  Template.options.picks = function(e, t) {
    return Session.get('picks');
  }
  Template.options.events({
    'keyup input[type="number"]': function (e, t) {
      //e.target.value = (e.target.value || '1').replace(/[^\d]/i, '');
    },
    'change input[type="number"]': function (e, t) {
      var val = parseInt(e.target.value);
      var max = parseInt(e.target.max || e.target.getAttribute('max'));
      var min = parseInt(e.target.min || e.target.getAttribute('min'));
      var ball = parseInt(e.target.ball || e.target.getAttribute('ball'));

      if(val > max) {
        e.target.value = max;
      }
      if(val < min) {
        e.target.value = min;
      }

      var picks = Session.get('picks')||[];
      picks[ball] = val;
      Session.set('picks', picks);
    },
    'click a.startRun': function (e, t) {
      if(!Session.get('playingNow')) {
        var playNext = function() {
          window.setTimeout(function() { 
            if(Session.get('playingNow')) {
              playLottery(playNext) 
            }
          }, (Session.get('speed') || 1) * 1000);
        }
        playLottery(playNext);
      }
      Session.set('playingNow', !Session.get('playingNow'));
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
